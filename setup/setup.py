# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 13:59:48 2020
Clase usada para crear y configurar un webDriver.
@author: Hugo
"""
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class Setup:

    def __init__(self, web_driver_name):
        self.driver = None
        ch_path = ("C:/Users/uaqhu/Documents/Selenium/Drivers/"
        "ChromeDriver/chromedriver.exe")
        try:
            if web_driver_name == "ch":
                options = Options()
                experimental_flags = ['same-site-by-default-cookies@1',
                                      ("cookies-without-same-"
                                       "site-must-be-secure@1")]
                chrome_prefs = {'browser.enabled_labs_experiments' :
                                experimental_flags}
                options.add_experimental_option('localState', chrome_prefs)
                options.headless = True
                new_driver = webdriver.Chrome(ch_path, chrome_options=options)
                self.driver = new_driver
                print("CHROME DRIVER ACTIVE")
            else:
                print("Opcion no valida seleccionada")
        except:
            print("Ocurrio un error al instanciar el WebDriver. FILE:Setup.py")

    # retorna el driver
    def get_driver(self):
        return self.driver
