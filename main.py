# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 18:38:36 2020
Clase principal usada para realizar el test.
@author: Hugo
"""
from poms.bandcamp_home_page import HomePage as hp
from setup.setup import Setup
from poms.results_page import ResultsPage as rp
from poms.player_page import PlayerPage as pp
import unittest


class BandCampTest(unittest.TestCase):

    def test_main(self):
        print('Iniciando Prueba....')
        try:
            url = "https://www.bandcamp.com/"
            chrome = Setup("ch")
            chrome = chrome.get_driver()
            chrome.get(url)

            home_page = hp(chrome)
            self.assertIsNotNone(home_page.search_word(),
                                 "No se encontro la barra de busqueda")

            results_page = rp(chrome)
            self.assertGreater(results_page.delete_invalid_options(), 0,
                               "No hay resultados de tu busqueda")
            self.assertTrue(results_page.validate_title(),
                            "Ningun resultado concuerda con tu busqueda")
            self.assertTrue(results_page.choose_an_option(),
                            "Error al intentar ingresar a la seleccion")

            player_page = pp(chrome)

            self.assertTrue(player_page.ask_user_to_play(),
                            "No se pudo reproducir la cancion")

            chrome.quit()
        except Exception as e:
            print(e)


if __name__ == "__main__":
    unittest.main()
