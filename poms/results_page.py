# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 20:28:44 2020
@author: Hugo

Clase usada para interactuar y manipular la pagina de resultados despues de
realizar una busqueda en la pagina principal.

"""

from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class ResultsPage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 20)
        self.xpath_titles = "//div[@class='heading']"
        self.xpath_types = "//div[@class='itemtype']"
        self.xpath_items_clickable = "//img"
        self.xpath_additional_info = "//div[@class='subhead']"
        self.xpath_input_search = "//input[@id='q']"
        self.types = []
        self.titles = []
        self.items = []
        self.additional_info = []

    # Elimina elementos de la lista de resultados que no tienen reproductor.
    def delete_invalid_options(self):
        print("Eliminando opciones no validas")
        self.get_web_elements_list()
        index = 0
        while index < len(self.types):
            if self.types[index].text != "TRACK" \
                    and self.types[index].text != "ALBUM" \
                    and self.types[index].text != "ARTIST":

                del self.types[index]
                del self.titles[index]
                del self.items[index]
                del self.additional_info[index]
            index += 1
        return len(self.titles)

    # Llena las listas con todos los elementos de la pagina.
    def get_web_elements_list(self):

        self.titles = self.driver.find_elements_by_xpath(
            self.xpath_titles)
        self.types = self.driver.find_elements_by_xpath(
            self.xpath_types)
        self.items = self.driver.find_elements_by_xpath(
            self.xpath_items_clickable)
        self.additional_info = self.driver.find_elements_by_xpath(
            self.xpath_additional_info)

    # Imprime los resultados ya depurados
    def print_results(self):
        print("Resultados:")

        for x in self.titles:
            index = self.titles.index(x)

            print("-----------------------------------")
            print("[", index, "] ", x.text)
            print("Type: ", self.types[index].text)
            print("Info:", self.additional_info[index].text)

    # Da click en la opcion seleccionado por el usuario
    def click_on_option(self, opc):
        try:
            self.items[opc-1].click()
            return True
        except NoSuchElementException:
            return False

    # Despliega todas los resultados validos, y pide al usuario que
    # seleccione una opcion para continuar.
    def choose_an_option(self):

        while True:
            self.print_results()

            print("INGRESE OPCION '100' para salir")

            opc = int(input("Seleccione una opcion\n"))

            if opc == 100:
                break
            elif opc <= len(self.titles)-1:
                break

        if opc != 100:
            flag = self.click_on_option(opc)
        return flag

    # Valida que haya coincidencias en los resultados
    def validate_title(self):

        search = self.driver.find_element_by_xpath(
            self.xpath_input_search).get_attribute('value')

        for x in self.titles:

            if search in x.text:
                print("Hay coincidencias en los resultados.")
                return True
        return False
