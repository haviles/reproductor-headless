# -*- coding: utf-8 -*-
"""
Created on Sun Apr  5 13:51:54 2020
Clase usada para interactuar con la pagina que tiene el reproductor,
despues de seleccionar la opcion en la
pagina anterior.

@author: Hugo
"""
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time

class PlayerPage:

    def __init__(self, driver):
        self.driver = driver
        self.xpath_play_bttn = "//div[@class='playbutton']"
        self.xpath_title_song = "//h2[@class='trackTitle']"
        self.wait = WebDriverWait(driver, 20)

    # Pregunta al usuario si desea reproducir la cancion y = si, n = no
    def ask_user_to_play(self):
        while True:
            opc = input("¿Desea Reproducir la cancion? [y/n]: \n")
            if opc == "y":
                flag = self.play_song()
                return flag
            elif opc == "n":
                return True
            else:
                print("Ingrese una opcion valida.")

    # Reproduce la cancion
    def play_song(self):
        try:
            self.wait.until(ec.presence_of_all_elements_located(
                (By.XPATH, self.xpath_title_song)))
            print("Reproduciendo...", self.driver.find_element_by_xpath(
                self.xpath_title_song).text)
            bttn_play = self.driver.find_element_by_xpath(self.xpath_play_bttn)
            bttn_play.click()
            time.sleep(15) #Usado para escuchar la rola antes de cerrar driver
            return True
        except NoSuchElementException:
            return False
