# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 18:38:36 2020
Clase usada para manipular la pagina principal (https://www.bandcamp.com/)

@author: Hugo
"""

class HomePage:
    driver = None
    name_search_bar = "q"

    def __init__(self, driver):
        self.driver = driver

    # Pregunta al usuario que desea buscar, envia el valor al input y lo busca.
    def search_word(self):

        input_search = self.driver.find_element_by_name(self.name_search_bar)
        busqueda = input("¿Qué desea buscar?\n")
        input_search.send_keys(busqueda)
        input_search.submit()
        return input_search
